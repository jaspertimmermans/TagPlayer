﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Media.Core;
using Windows.Media.Playback;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace TagPlayer_UWP
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();

        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            await SetLocalMedia();
        }

        private ObservableCollection<string> _items = new ObservableCollection<string>();

        public ObservableCollection<string> Items
        {
            get { return this._items; }
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            // Instead of hard coded items, the data could be pulled 
            // asynchronously from a database or the internet.
            Items.Add("90s");
            Items.Add("Acid");
            Items.Add("Ambient");
            Items.Add("Bouncy");
            Items.Add("Breaky");
            Items.Add("Bright");
            Items.Add("Cheesy");
            Items.Add("Chill");
            Items.Add("Clean");
            Items.Add("Dark");
            Items.Add("Dialog Female");
            Items.Add("Dialog Male");
            Items.Add("Dirty");
            Items.Add("Disco");
            Items.Add("Distorted");
            Items.Add("Downtempo");
            Items.Add("Dub");
            Items.Add("Epic");
            Items.Add("Ethereal");
            Items.Add("Funky");
            Items.Add("Glitchy");
            Items.Add("Groovy");
            Items.Add("Happy");
            Items.Add("Hard");
            Items.Add("HiHats");
            Items.Add("Industrial");
            Items.Add("Jackin'");
            Items.Add("Jazzy");
            Items.Add("Latin");
            Items.Add("Mainstream");
            Items.Add("Massive");
            Items.Add("Melancholic");
            Items.Add("Mellow");
            Items.Add("Melodic");
            Items.Add("Minimalistic");
            Items.Add("Old-Skool");
            Items.Add("Rolling");
            Items.Add("Sad");
            Items.Add("Sample Female");
            Items.Add("Sample Male");
            Items.Add("Sexy");
            Items.Add("Soulful");
            Items.Add("Spacey");
            Items.Add("Techy");
            Items.Add("Throaty");
            Items.Add("Tight");
            Items.Add("Tribal");
            Items.Add("Trippy");
            Items.Add("Underground");
            Items.Add("Uplifting");
            Items.Add("Vocal");
            Items.Add("Vocal Female");
            Items.Add("Vocal Male");
            Items.Add("Wavey");
            Items.Add("Weird");
        }

        async private System.Threading.Tasks.Task SetLocalMedia()
        {
            var openPicker = new Windows.Storage.Pickers.FileOpenPicker();

            openPicker.FileTypeFilter.Add(".wmv");
            openPicker.FileTypeFilter.Add(".mp4");
            openPicker.FileTypeFilter.Add(".wma");
            openPicker.FileTypeFilter.Add(".mp3");

            var file = await openPicker.PickSingleFileAsync();

            // mediaPlayer is a MediaPlayerElement defined in XAML
            if (file != null)
            {
                mediaPlayer.Source = MediaSource.CreateFromStorageFile(file);

                mediaPlayer.MediaPlayer.Play();
            }
        }
    }
}
